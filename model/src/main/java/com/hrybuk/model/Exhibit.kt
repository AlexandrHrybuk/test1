package com.hrybuk.model

class Exhibit(val title: String, val images: List<String>)