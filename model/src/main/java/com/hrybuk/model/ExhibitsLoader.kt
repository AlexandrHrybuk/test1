package com.hrybuk.model

interface ExhibitsLoader {
    fun getExhibitList(): List<Exhibit>
}