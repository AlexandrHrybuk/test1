package com.hrybuk.test1

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainActivityViewModel
    private val adapter = ExhibitsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel =
                ViewModelProviders.of(this, MainActivityViewModelFactory(this)).get(MainActivityViewModel::class.java)
        setContentView(R.layout.main_activity)
        setUpViewRecycler()
        addListeners()
    }

    private fun setUpViewRecycler() {
        rvExhibitEntities.layoutManager = LinearLayoutManager(this)
        rvExhibitEntities.adapter = adapter
        val decoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        getDrawable(R.drawable.recycler_divider)?.let { drawable ->
            decoration.setDrawable(drawable)
        }
        rvExhibitEntities.addItemDecoration(decoration)
    }

    private fun addListeners() {
        viewModel.exhibitsLiveData.observe(this, Observer {
            it?.let { exhibits ->
                adapter.update(exhibits)
            }
        })
    }
}