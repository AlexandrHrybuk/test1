package com.hrybuk.test1

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.hrybuk.fileexhibitsloader.MockExhibitsLoader

class MainActivityViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            MainActivityViewModel(MockExhibitsLoader(context)) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}