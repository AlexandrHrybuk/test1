package com.hrybuk.test1

import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.hrybuk.model.Exhibit
import kotlinx.android.synthetic.main.exhibit_image_item.view.*
import kotlinx.android.synthetic.main.exhibit_item.view.*

class ExhibitsAdapter : RecyclerView.Adapter<ExhibitsAdapter.ExhibitViewHolder>() {

    private var exhibitEntities = arrayListOf<Exhibit>()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ExhibitViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.exhibit_item, parent, false)
        return ExhibitViewHolder(view)
    }

    override fun getItemCount(): Int = exhibitEntities.size

    override fun onBindViewHolder(holder: ExhibitViewHolder, position: Int) {
        holder.bind(exhibitEntities[position])
    }

    fun update(entities: List<Exhibit>) {
        this.exhibitEntities = ArrayList(entities)
    }


    inner class ExhibitViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(exhibit: Exhibit) {
            itemView.tvTitle.text = exhibit.title
            itemView.rvImages.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            itemView.rvImages.adapter = ExhibitImagesAdapter(exhibit.images, exhibit.title)
        }


        inner class ExhibitImagesAdapter(
            private val imageUrls: List<String>,
            private val title: String
        ) :
            RecyclerView.Adapter<ExhibitImagesAdapter.ExhibitImageViewHolder>() {

            override fun onCreateViewHolder(parent: ViewGroup, position: Int): ExhibitImageViewHolder {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.exhibit_image_item, parent, false)
                return ExhibitImageViewHolder(view)
            }

            override fun getItemCount(): Int = imageUrls.size

            override fun onBindViewHolder(holder: ExhibitImageViewHolder, position: Int) {
                holder.bind(imageUrls[position])
            }


            inner class ExhibitImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
                fun bind(imageUrl: String) {
                    val progressDrawable = CircularProgressDrawable(itemView.context)
                    progressDrawable.strokeWidth = 8f
                    progressDrawable.centerRadius = 45f
                    progressDrawable.start()
                    val requestOptions = RequestOptions()
                        .transform(CenterCrop())
                        .placeholder(progressDrawable)
                        .error(R.drawable.baseline_broken_image_black_36)
                        .fallback(R.drawable.baseline_broken_image_black_36)
                    Glide.with(itemView.ivImage)
                        .load(imageUrl)
                        .apply(requestOptions)
                        .into(itemView.ivImage)
                    itemView.tvImageTitle.text = title
                }
            }
        }
    }
}