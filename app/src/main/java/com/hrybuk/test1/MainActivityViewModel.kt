package com.hrybuk.test1

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.hrybuk.model.Exhibit
import com.hrybuk.model.ExhibitsLoader

class MainActivityViewModel(exhibitsLoader: ExhibitsLoader) : ViewModel() {

    val exhibitsLiveData: MutableLiveData<List<Exhibit>> = MutableLiveData()

    init {
        exhibitsLiveData.postValue(exhibitsLoader.getExhibitList())
    }

}