package com.hrybuk.fileexhibitsloader

import android.content.Context
import com.google.gson.Gson
import com.hrybuk.model.Exhibit
import com.hrybuk.model.ExhibitsLoader
import java.io.IOException
import java.nio.charset.Charset

class MockExhibitsLoader(private val context: Context) : ExhibitsLoader {

    private val JSON_ASSET_NAME = "mock_json.json"

    override fun getExhibitList(): List<Exhibit> {
        val response = Gson().fromJson(getJson(), MockResponse::class.java)
        return response.list
    }

    private fun getJson(): String {
        try {
            val assetInputStream = context.assets.open(JSON_ASSET_NAME)
            val streamSize = assetInputStream.available()
            val buffer = ByteArray(streamSize)
            assetInputStream.read(buffer)
            assetInputStream.close()
            return String(buffer, Charset.forName("UTF-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            return ""
        }
    }
}